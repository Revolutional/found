//
//  ViewController.swift
//  Found
//
//  Created by GDD Student on 19/5/16.
//  Copyright © 2016 ITE. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func OpenMapsAppWithURL(sender: UIButton)
    {
        if let url = NSURL(string: "http://maps.apple.com/?q=Yosemite")
        {
            let app = UIApplication.sharedApplication()
            app.openURL(url)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

